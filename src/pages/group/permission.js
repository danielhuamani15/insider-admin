let permission = [
  {
    'name': 'Orden',
    'list_permission': [
      {
        'code': 'can_list_order',
        'name': 'Ver Listado'
      },
      {
        'code': 'change_order',
        'name': 'Modificar'
      }
    ]
  },
  {
    'name': 'Marcas',
    'list_permission': [
      {
        'code': 'can_list_influencer',
        'name': 'Ver Listado '
      },
      {
        'code': 'add_influencer',
        'name': 'Crear'
      },
      {
        'code': 'change_influencer',
        'name': 'Modificar'
      },
      {
        'code': 'delete_influencer',
        'name': 'Eliminar '
      }
    ]
  },
  {
    'name': 'Catalogo',
    'list_permission': [
      {
        'code': 'can_list_productclass',
        'name': 'Ver Listado '
      },
      {
        'code': 'add_productclass',
        'name': 'Crear'
      },
      {
        'code': 'change_productclass',
        'name': 'Modificar'
      },
      {
        'code': 'delete_productclass',
        'name': 'Eliminar '
      }
    ]
  },
  {
    'name': 'Categoría',
    'list_permission': [
      {
        'code': 'can_list_category',
        'name': 'Ver Listado '
      },
      {
        'code': 'add_category',
        'name': 'Crear'
      },
      {
        'code': 'change_category',
        'name': 'Modificar'
      },
      {
        'code': 'delete_category',
        'name': 'Eliminar '
      }
    ]
  },
  {
    'name': 'Grupo Atributos',
    'list_permission': [
      {
        'code': 'can_list_family',
        'name': 'Ver Listado '
      },
      {
        'code': 'add_family',
        'name': 'Crear'
      },
      {
        'code': 'change_family',
        'name': 'Modificar'
      },
      {
        'code': 'delete_family',
        'name': 'Eliminar '
      }
    ]
  },
  {
    'name': 'Atributos',
    'list_permission': [
      {
        'code': 'can_list_attribute',
        'name': 'Ver Listado '
      },
      {
        'code': 'add_attribute',
        'name': 'Crear'
      },
      {
        'code': 'change_attribute',
        'name': 'Modificar'
      },
      {
        'code': 'delete_attribute',
        'name': 'Eliminar '
      }
    ]
  },
  {
    'name': 'Cupones',
    'list_permission': [
      {
        'code': 'can_list_coupon',
        'name': 'Ver Listado '
      },
      {
        'code': 'add_coupon',
        'name': 'Crear'
      },
      {
        'code': 'change_coupon',
        'name': 'Modificar'
      },
      {
        'code': 'delete_coupon',
        'name': 'Eliminar '
      }
    ]
  },
  {
    'name': 'Clientes',
    'list_permission': [
      {
        'code': 'can_list_customer',
        'name': 'Ver Listado '
      }
    ]
  },
  {
    'name': 'Paginas',
    'list_permission': [
      {
        'code': 'can_list_pages',
        'name': 'Ver Listado '
      },
      {
        'code': 'add_pages',
        'name': 'Crear'
      },
      {
        'code': 'change_pages',
        'name': 'Modificar'
      },
      {
        'code': 'delete_pages',
        'name': 'Eliminar '
      }
    ]
  },
  {
    'name': 'Formulario',
    'list_permission': [
      {
        'code': 'can_list_contact',
        'name': 'Ver Listado '
      },
      {
        'code': 'change_contact',
        'name': 'Modificar'
      }
    ]
  },
  {
    'name': 'Enlaces',
    'list_permission': [
      {
        'code': 'can_list_configuration',
        'name': 'Ver Listado '
      },
      {
        'code': 'add_configuration',
        'name': 'Crear'
      }
    ]
  },
  {
    'name': 'Metodo de envío',
    'list_permission': [
      {
        'code': 'can_list_shippingmethod',
        'name': 'Ver Listado '
      },
      {
        'code': 'add_shippingmethod',
        'name': 'Crear'
      },
      {
        'code': 'change_shippingmethod',
        'name': 'Modificar'
      },
      {
        'code': 'delete_shippingmethod',
        'name': 'Eliminar '
      }
    ]
  },
  {
    'name': 'Usuarios',
    'list_permission': [
      {
        'code': 'can_list_user',
        'name': 'Ver Listado '
      },
      {
        'code': 'add_user',
        'name': 'Crear'
      },
      {
        'code': 'change_user',
        'name': 'Modificar'
      },
      {
        'code': 'delete_user',
        'name': 'Eliminar '
      }
    ]
  },
  {
    'name': 'Permisos',
    'list_permission': [
      {
        'code': 'can_list_group',
        'name': 'Ver Listado '
      },
      {
        'code': 'add_group',
        'name': 'Crear'
      },
      {
        'code': 'change_group',
        'name': 'Modificar'
      },
      {
        'code': 'delete_group',
        'name': 'Eliminar '
      }
    ]
  }
]
export default permission
