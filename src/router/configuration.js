import configuration from '@/pages/configuration/configuration'
import shippingCostList from '@/pages/configuration/shippingCostList'
import shippingCostoUpdate from '@/pages/configuration/shippingCostoUpdate'
import shippingMethodList from '@/pages/configuration/shippingMethodList'
import shippingMethodUpdate from '@/pages/configuration/shippingMethodUpdate'
import shippingMethodCreate from '@/pages/configuration/shippingMethodCreate'
import middlewareAuth from '@/middleware/auth'

const configurationRouter = [
  {
    path: 'configuration',
    name: 'configuration',
    component: configuration,
    beforeEnter: middlewareAuth
  },
  {
    path: 'shipping-method/crear/',
    name: 'shipping_method_create',
    component: shippingMethodCreate,
    beforeEnter: middlewareAuth
  },
  {
    path: 'shipping-method/shipping-cost/:id/',
    name: 'shipping_cost',
    component: shippingCostList,
    beforeEnter: middlewareAuth
  },
  {
    path: 'shipping-method/shipping-cost/:id_extra/:id/update',
    name: 'shipping_cost_update',
    component: shippingCostoUpdate,
    beforeEnter: middlewareAuth
  },
  {
    path: 'shipping-method',
    name: 'shipping_method',
    component: shippingMethodList,
    beforeEnter: middlewareAuth
  },
  {
    path: 'shipping-method/:id',
    name: 'shipping_method_update',
    component: shippingMethodUpdate,
    beforeEnter: middlewareAuth
  }

]

export default configurationRouter
