import groupList from '@/pages/group/groupList'
import groupCreate from '@/pages/group/groupCreate'
import groupUpdate from '@/pages/group/groupUpdate'
import middlewareAuth from '@/middleware/auth'

const groupRouter = [
  {
    path: 'group',
    name: 'group',
    component: groupList,
    beforeEnter: middlewareAuth
  },
  {
    path: 'group/crear',
    name: 'group_create',
    component: groupCreate,
    beforeEnter: middlewareAuth
  },
  {
    path: 'group/:id/actualizar',
    name: 'group_update',
    component: groupUpdate,
    beforeEnter: middlewareAuth
  }
]

export default groupRouter
