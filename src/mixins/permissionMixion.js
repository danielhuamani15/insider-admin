import {getPermissions, getIsSuperUser} from '@/utils/auth'

const permission = {
  data () {
    return {
    }
  },
  methods: {
    getPermission (permission) {
      return (getPermissions().indexOf(permission) !== -1) || getIsSuperUser()
    }
  }
}
export default permission
